const kS = 2.29;
const kV = 0.796;
const kA = 0.0192;
const kTrackWidth = 1.972; // ft
const M_2_FT = 3.281;
const T_SLICE = 0.02;
const MAX_VOLTS = 12;

// V = kS * Math.sign(d_) + kV * d_ + kA * d__

function clamp(v, min, max) {
  return Math.min(max, Math.max(min, v));
}

function driveMotor(v, p, t) {
  if (v >= -kS && v <= kS) {
    p.d__ =  -kV * p.d_/ kA;
  } else {
    p.d__ = (v - kS * Math.sign(p.d_) - kV * p.d_) / kA;
  }
  p.d_ += p.d__ * t;
  return p.d_ * t / M_2_FT;
}

class RobotSim {
  constructor() {
    this.x = 0;
    this.y = 0;
    this.angle = 0;
    this.width = kTrackWidth / M_2_FT;
    this.t = 0;
    this.period = T_SLICE;
    this._lmv = 0;
    this._rmv = 0;
    this._lm = {d_: 0, d__: 0};
    this._rm = {d_: 0, d__: 0};
    this.onArcadeDrive = null;
  }
  get gyro() {
    return 180 * this.angle / Math.PI;
  }
  tick() {
    const arcadeDriveEvent = { speed: 0, rotation: 0};
    if (this.onArcadeDrive) this.onArcadeDrive.call(null, arcadeDriveEvent);
    const ARCADE_DRIVE_DB = 0.1;
    const speed = Math.sign(arcadeDriveEvent.speed) * (clamp(Math.abs(arcadeDriveEvent.speed), ARCADE_DRIVE_DB, 1) - ARCADE_DRIVE_DB) / (1 - ARCADE_DRIVE_DB);
    const rot = Math.sign(arcadeDriveEvent.rotation) * (clamp(Math.abs(arcadeDriveEvent.rotation), ARCADE_DRIVE_DB, 1) - ARCADE_DRIVE_DB) / (1 - ARCADE_DRIVE_DB);
    const max = (speed < 0 ? -1 : 1) * Math.max(Math.abs(speed), Math.abs(rot));
    let r, l;
    if ((speed >=0) == (rot >= 0)) {
      l = max; r = speed - rot;
    } else {
      l = speed + rot; r = max;
    }
    this._lmv = MAX_VOLTS * clamp(l, -1, 1);
    this._rmv = MAX_VOLTS * clamp(r, -1, 1);
    this._applyVolts();
    this.t += this.period;
  }
  _applyVolts() {
    const dl = driveMotor(this._lmv, this._lm, this.period);
    const dr = driveMotor(this._rmv, this._rm, this.period);
    if (dl == dr) {
      this.x += Math.cos(this.angle) * dl;
      this.y -= Math.sin(this.angle) * dl;
    } else {
      const r = this.width / 2;
      // arc length = alpha * r
      // alpha = -dl / (r + p) = dr / (r - p) 
      const p = r * (dl + dr) / (dl - dr);
      const alpha = Math.abs(r + p) > 1e-10 ?
       -dl / (r + p) : dr / (r - p);
      const px = this.x - p * Math.sin(this.angle);
      const py = this.y - p * Math.cos(this.angle);
      const cosAlpha = Math.cos(alpha);
      const sinAlpha = Math.sin(alpha);
      const x = cosAlpha * this.x - sinAlpha * this.y +
        (-px * cosAlpha + py * sinAlpha + px);
      const y = sinAlpha * this.x + cosAlpha * this.y +
        (-px * sinAlpha - py * cosAlpha + py);
      this.x = x;
      this.y = y;
      this.angle += alpha;
      if (this.angle < -Math.PI) this.angle += 2*Math.PI;
      else if (this.angle > Math.PI) this.angle -= 2*Math.PI;
    }
  }
  simulate(t) {
    while (this.t < t) {
      this.tick();
    }
  }
}

class Drive {
  constructor() {
    this.P = 0.08;
    this.I = 0.01;
    this.D = 0.00;
    this.integral = 0;
    this.previous_error = 0;
    this.setpoint = 0;
    this.gyro = null;
    this.period = T_SLICE;
  }

  calc() {
    const error = this.setpoint - this.gyro.getAngle(); // Error = Target - Actual
    this.integral += (error * this.period); // Integral is increased by the error*time (which is .02 seconds using normal IterativeRobot)
    const derivative = (error - this.previous_error) / this.period;
    return this.P * error + this.I * this.integral + this.D * derivative;
  }
}

let inPID = false;

let robot = new RobotSim();
robot.onArcadeDrive = e => {
  if (joy.button1) {
    if (!inPID) {
      drive.setpoint = robot.gyro + 60;
      inPID = true;
    }
    const r = -drive.calc();
    e.rotation = clamp(r, -0.5, 0.5);
    return;
  }
  inPID = false;
  e.speed = joy.y;
  e.rotation = joy.z;
};

let drive = new Drive();
drive.gyro = { getAngle() { return robot.gyro; } };


var joy = {y: 0, z: 0};

function updateRobotPosition() {
  const b = document.getElementById("box");
  b.setAttribute("position", `${robot.x*M_2_FT} 0.5 ${robot.y*M_2_FT}`);
  b.setAttribute("rotation", `0 ${robot.angle*180/Math.PI} 0`);
}


function simulate() {
  const current = (Date.now() - start) / 1000;
  robot.simulate(current);
}

const start = Date.now();
setInterval(function () {
  simulate();
  updateRobotPosition();
}, 20);

